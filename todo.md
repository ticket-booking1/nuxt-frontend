# To DO
Create a simple concert ticket booking application, with the following features:
- Customer Login
- Customer Logout
- Customer View List of Concerts available
- Customers buy concert tickets
- Customer View order history

Stack options used:
- Next / Nuxt
- NestJS/Express
- MySQL / Postgre

Explain the why and why of using the stack.

The point is
- Working speed
- Objective Achievement
- Docker implementation
- Implementation of Docker Compose
- Use of Other Stacks
- Explanation regarding why, why use this stack

Points (+)
- How to deal with race conditions?
- Application created with docker
- There is documentation
- Can be hosted on free hosting (accessible to the public)