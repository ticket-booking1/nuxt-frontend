// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      apiBase: 'http://139.162.63.203:6868'
      // apiBase: 'http://localhost:8080'
    }
},
  modules: [
    // '@nuxtjs/tailwindcss', 
    '@pinia/nuxt', 
    '@pinia-plugin-persistedstate/nuxt',
    '@formkit/nuxt',
    '@nuxt/ui',
    // '@nuxtjs/date-fns'
  ],
  //  // To fix samesite console error
  //  piniaPersistedstate: {
  //   cookieOptions: {
  //     sameSite: "strict",
  //   },
  // },
})
