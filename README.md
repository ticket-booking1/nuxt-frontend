# nuxt-frontend
# To DO
Create a simple concert ticket booking application, with the following features:
- Customer Login
- Customer Logout
- Customer View List of Concerts available
- Customers buy concert tickets
- Customer View order history

Stack options used:
- Next / Nuxt
- NestJS/Express
- MySQL / Postgre

Explain the why and why of using the stack.

The point is
- Working speed
- Objective Achievement
- Docker implementation
- Implementation of Docker Compose
- Use of Other Stacks
- Explanation regarding why, why use this stack

Points (+)
- How to deal with race conditions?
- Application created with docker
- There is documentation
- Can be hosted on free hosting (accessible to the public)

# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on `http://localhost:3000`

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```
## STACK
- pinia
 this stack using for save state token user, credential like vuex in vuejs.
 this is true for single page applications but exposes your application to security vulnerabilities if it is server side rendered.
- nuxt/ui
this stack using for component UI

## Docker
```bash
docker build -t ticket-app:nginx .
docker run --name ticket-app-nginx -it -p 8080:80 ticket-app:nginx
```

## DEMO
[Booking Ticket Demo](http://139.162.63.203:8080)


Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## How to deal with race conditions?
Sequelize supports two ways of using transactions:

    Unmanaged transactions: Committing and rolling back the transaction should be done manually by the user (by calling the appropriate Sequelize methods).

    Managed transactions: Sequelize will automatically rollback the transaction if any error is thrown, or commit the transaction otherwise. Also, if CLS (Continuation Local Storage) is enabled, all queries within the transaction callback will automatically receive the transaction object.
    