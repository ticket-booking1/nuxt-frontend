import { defineStore } from 'pinia'
export const useUserStore = defineStore("user", () => {
  const user = ref(null)
  function setUser(ath) {
    user.value =  ath
  }

  // Resetting State
  function $reset() {
    user.value = null
  }

  return { user, setUser, $reset }

},
  
  {
    persist: {
      storage: persistedState.cookiesWithOptions({
        sameSite: 'strict',
      }),
    }
  }
)